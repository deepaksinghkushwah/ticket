import React from 'react';

function NotFound() {
    return <>
        <h1 className='text-danger'>404: Page not found</h1>
        <p>Requesting page does not found on server, please contact support or try again later.</p>
    </>;
}

export default NotFound;
