import React, { useState } from 'react';
import {FaSignInAlt} from 'react-icons/fa';
import {useSelector, useDispatch} from 'react-redux';
import { login } from '../features/auth/authSlice';
import { toast } from "react-toastify";
function Login() {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    password: '',
    password2: ''
  });

  const {email, password} = formData;
  const onChange = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  }

  const dispatch = useDispatch();
  const {user, isLoading, isSuccess, message} =  useSelector(state => state.auth);

  const onSubmit = (e) => {
    e.preventDefault();
    const userData = {
      email, password
    }
    dispatch(login(userData));
    
    
    
  }

  return <>
  <section className="heading">
    <h1><FaSignInAlt/> Login</h1>
    <p>Please login to get support</p>
  </section>
  <section className='form'>
    <form onSubmit={onSubmit}>
      
      <div className="form-group">
        <input type="text" name="email" className='form-control' id="email" value={email} onChange={onChange} placeholder="Enter your email" required />
      </div>

      <div className="form-group">
        <input type="password" name="password" className='form-control' id="password" value={password} onChange={onChange} placeholder="Enter your password" required />
      </div>

      
      <div className="form-group">
        <button type='submit' className='btn btn-block'>Register</button>
      </div>
    </form>
  </section>
  </>;
}

export default Login;
