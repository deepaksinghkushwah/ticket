import { configureStore } from '@reduxjs/toolkit';
import authReeducer from '../features/auth/authSlice';

export const store = configureStore({
  reducer: {
    auth: authReeducer,
  },
});